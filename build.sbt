
/* 
 * Seer Modules - Project Refs 
 */
val app = ProjectRef(file("seer"),"gdx_app_desktop")
val script = ProjectRef(file("seer"),"script")
val video = ProjectRef(file("seer"),"video")

val faff = project.in(file(".")).
  dependsOn(app, script, video).
  settings( Common.appSettings :_* )
