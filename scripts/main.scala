

class Main extends SeerActor {

  Keyboard.onKeyDown { 
    case '-' => installationMode() //TODO
    case '0' => blackout()
    
    case '1' => triangle()
    case '2' => addParticles()
    case '3' => justParticles()
    case '4' => grid()
    case 'p' => System.send("Grid","pluck")
    case '5' => grow()
    case '6' => video()
    case '7' => fadeOut()

  }

  Mouse.listen {
    case List(_,_,y:Float,_,dy:Float) if Keyboard.down.now == 'f' =>
      Camera.setFOV(y*67f)
  }


  def blackout(){
    if(si != null) si.cancel
    Camera.nav.quat.setIdentity()
    Camera.nav.pos.set(0,0,2)
    Camera.setFOV(67f)  
    System.broadcast(false)
    System.send("Blur", ("feedback", 0f, 1f))
    System.send("Grid", ("pluck", false)) 
  }

  def triangle(){
    blackout()
    System.send("Tri", true)
    System.send("Blur", ("feedback", 0.95f, 0.5f))
  }

  def addParticles(){
    System.send("Tri", true)
    System.send("Particles", 1f)
    System.send("Particles", true)
    System.send("Particles", ("lissajous",false))
    System.send("Blur", ("feedback", 0.95f, 0.5f))

  }

  def justParticles(){
    System.send("Tri", false)
    System.send("Particles", 1f)
    System.send("Particles", true)
    System.send("Particles", ("lissajous",true))
    System.send("Blur", ("feedback", 0.95f, 0.5f))
  }

  var s1:akka.actor.Cancellable = _
  def grid(){
    System.send("Blur", ("feedback", 0.95f, 0.5f))
    System.send("Grid", 0f)
    System.send("Grid", true)
    if(s1 != null) s1.cancel
    s1 = Schedule.over(10 seconds){ case t =>
      val f = Ease.sine(t).toFloat
      System.send("Grid", f)
      System.send("Particles", 1f-f)
      if(t == 1f) System.send("Particles", false)
    }
  }

  var s:akka.actor.Cancellable = _
  def grow(){
    System.send("Fennel", ("grow", 0f))
    System.send("Fennel", 1f)
    System.send("Fennel", true)
    if(s != null) s.cancel
    s = Schedule.over(10 seconds){ case t =>
      val f = Ease.sine(t).toFloat
      System.send("Fennel", ("grow", f))
      if(t == 1f) s = Schedule.over(10 seconds){ case t =>
        val f = Ease.sine(t).toFloat
        System.send("Grid", 1-f)
        if(t == 1f) System.send("Grid", false)
      }
    }
  }

  var s2:akka.actor.Cancellable = _
  def video(){
    System.send("Video", 0f)
    System.send("Video", true)
    if(s2 != null) s2.cancel
    s2 = Schedule.over(20 seconds){ case t =>
      val f = Ease.sine(t).toFloat
      System.send("Video", f)
      System.send("Fennel", 1f-f)

      System.send("Blur", ("feedback", 0.95f*(1f-f) + 0.45694444f*f, 0.5f*(1f-f) + 0.32111108f*f))
      if(t == 1f) System.send("Fennel", false)
    }

  }

  var s3:akka.actor.Cancellable = _
  def fadeOut(){
    if(s3 != null) s3.cancel
    s3 = Schedule.over(20 seconds){ case t =>
      System.send("Video", 1f-t)
      if(t == 1f) blackout()
    }
  }


  var si:akka.actor.Cancellable = _
  def installationMode(){
    triangle()    
    if(si != null) si.cancel
    si = Schedule.after(40 seconds){ 
      addParticles() 
      si = Schedule.after(40 seconds){
        justParticles()
        si = Schedule.after(50 seconds){
          System.send("Grid", ("pluck", true))
          grid() 
          si = Schedule.after(50 seconds){ 
            grow()
            si = Schedule.after(50 seconds){ 
              video()
              si = Schedule.after(50 seconds){ 
                installationMode()
              }
            }
          }
        }
      }
    }

  }
}





classOf[Main]