

class Blur extends SeerActor {

  var b:BlurNode = _
  var fb:FeedbackNode = _

  override def init(){
    b = new BlurNode
    b.size = 0.0005f //1/512f
    b.intensity = 0.65f //0.599f

    fb = new FeedbackNode(0.95f,0.5f)

    // RenderGraph.reset
    RootNode.outputs.clear
    // RootNode >> new ScreenNode
    // RootNode >> b >> new ScreenNode
    // RootNode >> b >> new FeedbackNode(0.95f,0.8f) >> new ScreenNode
    RootNode >> fb >> b >> new ScreenNode

    Schedule.oscillate(500 millis){ case t =>
      val s = Ease.cubicIn(t)*0.0025f + 0.0005f
      b.size = s.toFloat
    }
    Mouse.listen {
      case List(_, x:Float,y:Float,_,dy:Float) if Keyboard.down.now == 'b' => fb.setBlend(x,y); println(s"blend: $x $y")
      // case List(_,_,_,_,dy:Float) if Keyboard.down.now == 'b' => b.size += dy*0.01f
    }
  }

  override def receive = super.receive orElse {   
    case ("feedback", f:Float, f0:Float) => fb.setBlend(f,f0)
  }

}

classOf[Blur]