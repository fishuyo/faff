
import com.fishuyo.seer.world.particle._

class Grid extends SeerActor {
  active = false

  var pluck = false

  val n = 40
  val mesh = Plane.generateMesh(50,50,n,n,Quat.up)
  mesh.primitive = Lines
  val model = Model(mesh).translate(0,-1.5f,-25)
  val color2 = RGBA(50/255f, 240/255f, 150/255f, 1f)
  // val color = RGBA(87/255f, 56/255f, 250/255f, 1f)
  val color = RGBA(67/255f, 26/255f, 220/255f, 1f)
  model.material.color.set(color)
  model.material.transparent = true
  // model.material.color.set(90/255f, 250/255f, 225/255f, 1f)
  val fabric = new SpringMesh(model.mesh, 1f)

  fabric.pins += AbsoluteConstraint(fabric.particles(0), fabric.particles(0).position)
  fabric.pins += AbsoluteConstraint(fabric.particles(n), fabric.particles(n).position)
  fabric.pins += AbsoluteConstraint(fabric.particles.last, fabric.particles.last.position)

  override def receive = super.receive orElse {   
    case "pluck" => Run.animate {
      val i = util.Random.int(0, model.mesh.vertices.length)()
      model.mesh.vertices(i) += model.mesh.normals(i) * 1.5f
    }
    case f:Float => model.material.color.set(color*f)
    case ("pluck",b:Boolean) => pluck = b; println(b)
  }

  override def draw(){
    if(!active) return
    model.draw
  }

  var lpos = Vec2()
  var t = 0f
  override def animate(dt:Float){
    if(!active) return
    t += dt 
    implicit def f2i(f:Float) = f.toInt
    if( Mouse.status.now == "drag"){
      val vel = (Mouse.xy.now - lpos)/dt
      val r = Camera.ray(Mouse.x.now*Window.width, (1f-Mouse.y.now) * Window.height)
      r.o -= model.pose.pos 
      fabric.particles.foreach( (p) => {
        val t = r.intersectSphere(p.position, 0.25f)
        if(t.isDefined) p.applyForce(Vec3(vel.x,vel.y,0)*1000f)
      })
    }
    lpos = Mouse.xy.now

    // if(Keyboard.down.now == 'r') 
      fabric.particles.foreach{ case p => p.applyForce((p.initialPosition - p.position)*0.75f) }


    fabric.animate(dt)

    // every second, snap a random vertex along its normal
    if(pluck && t > 5f){
      t = 0f
      val i = util.Random.int(0, model.mesh.vertices.length)()
      model.mesh.vertices(i) += model.mesh.normals(i) * (util.Random.float()*1f)
    }
  }

}

classOf[Grid]