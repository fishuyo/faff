

class Tri extends SeerActor {
  active = true
  
  val mesh = Mesh()
  mesh.primitive = LineStrip
  mesh.maxVertices = 100
  val model = Model(mesh)
  model.material.lineWidth = 5

  val r = Random.vec3.map{ case v => v.z = 0; v.normalize }

  mesh.vertices ++= (0 until 3).map{case _ => Vec3()}
  mesh.vertices += mesh.vertices.head

  // Schedule.oscillate(500 millis){ case t =>
  //   val s = (t*15) + 3
  //   model.material.lineWidth = s.toInt
  // }

  val d = 10.seconds
  Schedule.every(d, 0 seconds){
    var dest = List(r(),r(),r())
    while(area(dest) < 1f) dest = List(r(),r(),r())
    val vs = mesh.vertices.map(Vec3(_))

    Schedule.over(d){ case t => 
      val l = Ease.sine(t).toFloat
      mesh.vertices.zip(vs).zip(dest).foreach { case ((v,v0),d) =>
        v.set(v0.lerp(d,l))
      }
      if(active) System.send("Particles", ("attractors", mesh.vertices.take(3).toList))
      mesh.update
    }
  }

  def area(l:List[Vec3]):Float = {
    if(l.length < 3) return 0f
    val b = l(1) - l(0)
    val bp = Vec3(b.y, -b.x, 0f).normalize
    val h = (l(1)-l(2)).dot(bp)
    0.5f * h * b.mag
  }

  override def draw(){
    if(!active) return
    model.draw
  }
}

classOf[Tri]