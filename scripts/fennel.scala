
import com.fishuyo.seer.world.branch._

class Fennel extends SeerActor {
  active = false

  val trees = (-1 to 1).map{ case x => new Tree(Pose(Vec3(x*0.75f,-1.5f,0),Quat.up))}
  trees.foreach{ case t => 
    t.trunk.thick = 4f
    t.trunk.length = 0.3f
    t.trunk.maxLength = 0.3f
  }
  
  generate()

  Mouse.listen {
    case List(_,_,y:Float,_,dy:Float) if Keyboard.down.now == 't' =>
      grow(y)
  }

  def grow(t:Float){
    trees.foreach { case tr => 
      ggrow(tr.trunk, t)
      // t.trunk.length = t
      // t.trunk.children.foreach
    }
  }

  def ggrow(b:Branch, t:Float){
    b.length = t * b.maxLength
    var tt = 0.001f
    if(t > 0.25f) tt = (t-0.25f)/0.75f
    b.children.foreach{ case b => ggrow(b,tt)}
  }


  def generate(){
    trees.foreach{ case t =>
      t.clear
      t.generate(7){ case b =>
        val lratio = 0.99f
        val tratio = 0.65f
        val twist = 1.3f //Random.float()
        val pos = b.pose.quat.toZ * b.length
        
        for(i <- 0 until 2){
          val e = Random.vec3()*0.4f
          e.z = twist
          val quat = Quat().fromEuler(e)
          val child = Branch(b, Pose(pos,quat))
          // child.damp = b.damp * 0.9f
          child.maxLength = b.maxLength * lratio
          child.length = b.length * lratio
          child.thick = b.thick * tratio
          b.children += child
        }
      }
    }
  }

  val mesh = Mesh()
  val lineModel = Model(mesh)
  val model = Model(Cylinder.generateMesh(0.01f,0.008f,2,10)) 
  val color = RGBA(20/255f, 200/255f, 110/255f, 1f)
  model.material.transparent = true
  // model.material.color.set(0.2f)
  model.material.color.set(color)


  override def receive = super.receive orElse {
    case f:Float => model.material.color.set(color*f)
    case "reset" => active = false
    case "generate" => generate()
    case ("grow", t:Float) => grow(t)
  }

  val lrhpos = Vec3()

  override def animate(dt:Float){
    if(!active) return
    // tree.trunk.applyForce(Vec3(2*Mouse.x.now-1,0,0))
    // tree.trunk.applyForce(Vec3(0,2*Mouse.y.now-1,0))
    // roots.trunk.applyForce(Vec3(2*Mouse.x.now-1,0,0))
    // roots.trunk.applyForce(Vec3(0,2*Mouse.y.now-1,0))

    // tree.trunk.grow(dt)
    trees.foreach{ case t =>
      t.trunk.applyForce(Random.vec3()*0.15f)
      t.trunk.step(dt)
      t.trunk.solve()
    }

  }

  override def draw(){
    if(!active) return
    // FPS.print
    // tree.trunk.draw(model)
    trees.foreach(_.draw(model))
    // roots.draw(model)
    // mesh.clear()
    // tree.lineMesh(mesh)
    // mesh.update()
    // lineModel.draw()

  }

}

classOf[Fennel]