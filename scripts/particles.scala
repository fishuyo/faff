
import com.fishuyo.seer.world.particle._
import com.fishuyo.seer.world.field._
import rx._
import scala.concurrent.duration._

class Particles extends SeerActor {
  active = false
  
  Gravity.set(0f,0f,0f)

  var lissajous = false
  val attractors = (0 until 3).map{ case _ => 
    val a = new Attractor
    a.radius = 0f
    a.strength = 0.25f
    a
  }
  val att = new Attractor
  att.radius = 0.1f
  att.strength = 0.5f

  Mouse.listen {
    case List(_,_,_,_,dy:Float) if Keyboard.down.now == 'i' =>
      attractors.foreach { case a => a.radius += dy }
      println(s"radius: ${attractors.head.radius}")
    case List(_,_,_,_,dy:Float) if Keyboard.down.now == 'o' =>
      attractors.foreach { case a => a.strength += dy }
      println(s"strength: ${attractors.head.strength}")

  }


  val emitter = new ParticleEmitter(30000) {
    val mesh = Mesh()
    mesh.maxVertices = 30000
    mesh.primitive = Points
    val model = Model(mesh)
    // model.pose.pos.set(0f,1f,1f)
    model.material.transparent = true
    model.material.color.set(0.1f)
    override def animate(dt:Float){
      attractors.foreach{ case a => a(particles) }
      // if(lissajous) att(particles)
      super.animate(dt)
      mesh.clear
      mesh.vertices ++= particles.map( _.position )
      mesh.update
    }
    override def draw(){
      model.draw
    }
  }

  val n = 40
  val field = new VecField3D(n,Vec3(0f),2f)
  var updateField = false
  emitter.ttl = 0f
  emitter.damping = 100f
  // emitter.field = Some(field)
  emitter.fieldAsForce = true
  randomizeField()

  (0 until 10000).foreach{ case _ => emitter += Particle(Random.vec3()) }
  // emitter.particles.foreach{ case p => p.mass = 1f + Random.float()*10f }

  def randomizeField(){
    for( z<-(0 until n); y<-(0 until n); x<-(0 until n)){
      val cen = field.centerOfBin(x,y,z).normalize
      // field.set(x,y,z,Vec3(0))
      if(emitter.fieldAsForce) field.set(x,y,z, Random.vec3())
      else field.set(x,y,z, Random.vec3()*0.01f)
      //field.set(x,y,z, cen * -.1f)
      //field.set(x,y,z, Vec3(x,y,z).normalize * .1f)
      //field.set(x,y,z, Vec3( -cen.z + -cen.x*.1f, -cen.y, cen.x ).normalize * .1f )
      //field.set(x,y,z, Vec3( math.sin(cen.x*3.14f), 0, math.cos(cen.z*3.14f) ).normalize * .1f)  
      //field.set(x,y,z, Vec3( cen.x, y/10.0f, cen.z).normalize * .1f )
      //field.set(x,y,z, Vec3(0,.1f,0) )
      //field.set(x,y,z, (Vec3(0,1,0)-cen).normalize * .1f )
    }
  }

  // add a particle every 10ms
  var s:akka.actor.Cancellable = new Schedulable
  def startSlow(){
    s.cancel
    s = Schedule.every(200.millis){
      emitter += Particle(Random.vec3()*3) //Vec3(Random.float(),2,0))
    }
  }
  def start(){
    s.cancel
    s = Schedule.every(10.millis){
      emitter += Particle(Random.vec3()*3) //Vec3(Random.float(),2,0))
    }
  }
  def stop() = s.cancel


  override def receive = super.receive orElse {           // handles messages from other actors
    case f:Float => emitter.model.material.color.set(f*0.1f)
    case "clear" => stop(); emitter.particles.clear; randomizeField(); active = false; emitter.model.material.color.set(0.75f)
    case "start" => start(); active = true
    case "slow" => startSlow(); active = true
    case "reset" => self ! "clear"
    case ("lissajous", b:Boolean) => lissajous = b; attractors.foreach{ case a => 
      if(b){
        a.strength = 0.5f 
        a.radius = 0f
      } else {
        a.strength = 0.25f
        a.radius = 0f
     }
    }
    case ("attractors", vs:List[Vec3]) => attractors.zip(vs).foreach { case (a,v) => a.position.set(v); a.setVelocity(Vec3(0)) }
  }

  var xt = 0f
  override def animate(dt:Float){
    if(!active) return
    if(lissajous){
      // val x = A\sin(at+\delta )
       // y=B\sin(bt),
      val timeStep = Integrators.dt
      val steps = ( (dt+xt) / timeStep ).toInt
      xt += dt - steps * timeStep

      for( t <- (0 until steps)){
        att(attractors)
        attractors.combinations(2).foreach{ case as => as.head(as.tail); as.tail.head(Seq(as.head)) }
        attractors.foreach{ case a => a.applyDamping(2f); a.step()}
      }
    }
    emitter.animate(dt)
  }
  override def draw(){
    // com.badlogic.gdx.Gdx.gl.glEnable( 0x8642 ); // enable gl_PointSize ???
    // GL11.glEnable(GL11.GL_LINE_SMOOTH);
    // GL11.glEnable(GL11.GL_POINT_SMOOTH);
    if(!active) return
    emitter.draw()
    attractors.foreach{ case a => Sphere().scale(0.005f).translate(a.position).draw }
  }

  override def unload(){
    super.unload()
    s.cancel
  }


}

classOf[Particles]