
import com.fishuyo.seer.video._

class Video extends SeerActor {
  active = false

  // val node = RenderNode()
  // scene = node.renderer.scene
  // node.renderer.camera = Camera
  // node.renderer.scene += this

  var video1:VideoTexture = _

  override def receive = super.receive orElse {
    case f:Float => if(video1 != null){
      video1.quad.material.transparent = true
      video1.quad.material.color.set(0f)
      video1.quad.material.textureMix = f
    }
  }

  override def init(){
    video1 = new VideoTexture("/Users/fishuyo/Desktop/vids/fennel2.mp4")
    video1.setRate(1f)
    video1.setVolume(0.0f)
    video1.setAudioChannel(0)
    video1.quad.translate(0,0,1.2f)
    // RenderGraph += node
  }

  override def draw(){
    // node.renderer.active = active
    if(video1 != null) video1.play(active)
    if(!active) return
    video1.update
    video1.draw
  }

  override def unload(){
    super.unload
    if(video1 != null) video1.dispose
    // RenderGraph -= node
  }

  // override def receive = super.receive orElse {

  // }

}

classOf[Video]